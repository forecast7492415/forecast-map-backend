from pydantic_settings import BaseSettings
from pydantic import PostgresDsn
import redis
import pytz
class Settings(BaseSettings):
    database_url: PostgresDsn

rd = redis.Redis(host="redis", port=6379, db=0)

almaty_timezone = pytz.timezone('Asia/Almaty')

settings = Settings()


