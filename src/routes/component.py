from fastapi import APIRouter
from ..service import componentService

router = APIRouter()

@router.get("")
async def get_components():
    response = await componentService.get_components()

    return response

@router.get("/average")
async def get_average_trends():
    response = await componentService.get_average_trends()
    return response