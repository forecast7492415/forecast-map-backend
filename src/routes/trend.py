from fastapi import APIRouter
from ..service import trendService
router = APIRouter()

@router.get("/components/{component_id}")
async def get_trends_by_component(component_id : int):
    response = await trendService.get_trend_by_component(component_id)
    return response
