from tortoise.models import Model
from tortoise import fields

class ComponentVariable(Model):
    id = fields.IntField(pk=True)
    variable = fields.ForeignKeyField('models.Variable', 'component_variables')
    component = fields.ForeignKeyField('models.Component', 'component_variables')

    class Meta:
        table = "component_variables"