from tortoise.models import Model
from tortoise import fields

class Variable(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=512)
    description = fields.CharField(max_length=1024)

    class Meta:
        table = "variables"
