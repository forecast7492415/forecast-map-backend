from tortoise.models import Model
from tortoise import fields

class Component(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=100)
    latitude = fields.FloatField()
    longitude = fields.FloatField()
    type = fields.CharField(max_length=100)

    class Meta:
        table = "components"
