from tortoise.models import Model
from tortoise import fields

class Trend(Model):
    id = fields.IntField(pk=True)
    l = fields.SmallIntField()
    t = fields.DatetimeField()
    v = fields.FloatField()
    q = fields.IntField()

    class Meta:
        table = "trends"
        unique_together = [("id", "l", "t")]
