import json

from ..models import ComponentVariable, Variable, Trend
from ..config import rd
from datetime import datetime
import asyncio
class TrendService:

    variable_dictionary = {
        "Суточная выработка электроэнергии kWh": "Суточная выработка электроэнергии, кВт⋅ч",
        "Месячная выработка электроэнергии kWh": "Месячная выработка электроэнергии, МВт⋅ч",
        "Общая выработка электроэнергии kWh": "Общая выработка электроэнергии, МВт⋅ч",
        "Сокращение выбросов СО2 , кг": "Сокращение выбросов СО2, кг",
        "Наработка по часам общая, ч.": "Количество часов",
        "Общая активная мощность, КВатт": "Общая текущая мощность, кВт",
        "Годовая выработка электроэнергии kWh": "Годовая выработка электроэнергии, МВт⋅ч",
        "Temperature": "Температура, °C",
        "DC_bus_volt": "Напряжение комбайнер бокса, Вольт",
        "Total_Current": "Сила тока, А",
        "Total_DC_Power": "Мощность, Вт",
        "Day_power": "Выработка за сутки, кВт⋅ч",
        "TOtal_power": "Общая выработка, кВт⋅ч",
    }

    avoid_variables = [
        "Коэфициент мощности",
        "Частота сети, Гц",
        "Температура трансформаторного масла С",
        "Температура обмотки С",
        "Напряжение фаз А-C, Вольт",
        "Общая реактивная мощность, Квар",
        "Напряжение фаз А-В, Вольт",
        "Напряжение фаз С-А, Вольт"
    ]

    async def get_trend_by_component(self, component_id):
        cache = rd.get(component_id)
        if cache:
            return json.loads(cache)
        component_variables = await ComponentVariable.filter(component_id=component_id)
        list_of_trends_dto = []
        for component_variable in component_variables:
            variables = await Variable.filter(id=component_variable.variable_id).order_by('id')
            tasks = []
            for variable in variables:
                task = Trend.filter(id=variable.id).order_by('-t').first()
                tasks.append(task)
            trends = await asyncio.gather(*tasks)
            for trend, variable in zip(trends, variables):
                if trend == None:
                    continue
                if variable.description in self.avoid_variables:
                    continue
                if variable.description in self.variable_dictionary.keys():
                    variable.description = self.variable_dictionary[variable.description]
                dt_object = datetime.fromisoformat(str(trend.t))
                hours = dt_object.hour
                minutes = dt_object.minute
                time = f"{hours:02}:{minutes:02}"
                list_of_trends_dto.append(
                    {
                        "description": variable.description,
                        "v": float("{:.1f}".format(trend.v)),
                        "t": time
                    }
                )
        rd.set(component_id, json.dumps(list_of_trends_dto, default=str))
        rd.expire(component_id, 60 * 30)
        print(f"Trend of component with id {component_id} is ready")
        return list_of_trends_dto



trendService = TrendService()