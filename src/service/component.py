import json

from ..models import Component
from .trend import trendService
from ..config import rd, almaty_timezone
import asyncio
from collections import defaultdict
from datetime import datetime
import time

class ComponentService:
    async def store_components_with_trends(self):
        start_time = time.time()
        components = await Component.all()
        components_dto = []
        tasks = []

        for component in components:
            task = trendService.get_trend_by_component(component.id)
            tasks.append(task)
        print("started making requests")

        trends = await asyncio.gather(*tasks)

        for component in components:
            components_dto.append(
                {
                    "id": component.id,
                    "name": component.name,
                    "latitude": component.latitude,
                    "longitude": component.longitude,
                    "type": component.type
                }
            )

        for component_dto, trend in zip(components_dto, trends):
            component_dto['trend'] = trend

        print("all tasks are completed")

        rd.set("components", json.dumps(components_dto, default=str))
        rd.expire("components", 10800)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Function executed in {execution_time:.2f} seconds.")
        return components_dto

    async def get_components_with_trends(self):
        cache = rd.get("components")
        if cache:
            return json.loads(cache)
        components_dto = await self.store_components_with_trends()
        return components_dto

    async def get_components(self):
        components_with_trends = await self.get_components_with_trends()

        sum_cb = 0
        cnt_cb = 0
        sum_scu = 0
        cnt_scu = 0

        for component_with_trend in components_with_trends:
            if component_with_trend['type'] == 'CB':
                for trend_item in component_with_trend['trend']:
                    if trend_item['description'] == 'Мощность, Вт':
                        sum_cb += trend_item['v']
                cnt_cb += 1
                continue
            if component_with_trend['type'] == 'SCU':
                for trend_item in component_with_trend['trend']:
                    if trend_item['description'] == 'Суточная выработка электроэнергии, кВт⋅ч':
                        sum_scu += trend_item['v']
                cnt_scu += 1
                continue

        avg_cb = sum_cb / cnt_cb
        avg_scu = sum_scu / cnt_scu
        response = []
        for component_with_trend in components_with_trends:
            color = ""
            if component_with_trend['type'] == 'CB':
                for trend_item in component_with_trend['trend']:
                    if trend_item['description'] == 'Мощность, Вт':
                        color = self.define_color(avg_cb, trend_item['v'])
            elif component_with_trend['type'] == 'SCU':
                for trend_item in component_with_trend['trend']:
                    if trend_item['description'] == 'Суточная выработка электроэнергии, кВт⋅ч':
                        color = self.define_color(avg_scu, trend_item['v'])
            elif component_with_trend['type'] == 'METEO':
                color = 'BLACK'
            response.append(
                {
                    "id": component_with_trend['id'],
                    "name": component_with_trend['name'],
                    "latitude": component_with_trend['latitude'],
                    "longitude": component_with_trend['longitude'],
                    "type": component_with_trend['type'],
                    "color": color
                }
            )
        return response

    async def get_average_trends(self):
        components_with_trends = await self.get_components_with_trends()
        scu_components = []
        for components_with_trend in components_with_trends:
            if components_with_trend['type'] == 'SCU':
                scu_components.append(components_with_trend)
        scu_dict = defaultdict(float)
        for component in scu_components:
           for trend_item in component['trend']:
               scu_dict[trend_item['description']] += trend_item['v']
        response = []

        sum_instead_of_average_set = {
            "Суточная выработка электроэнергии, кВт⋅ч",
            "Месячная выработка электроэнергии, МВт⋅ч",
            "Общая выработка электроэнергии kWh",
            "Сокращение выбросов СО2, кг",
            "Общая текущая мощность, кВт⋅ч",
            "Годовая выработка электроэнергии, МВт⋅ч",
            # "Общая реактивная мощность"
        }

        for key in scu_dict.keys():
            current_time = datetime.now(almaty_timezone)
            hours = current_time.hour
            minutes = current_time.minute
            time = f"{hours:02}:{minutes:02}"
            scu_value = scu_dict[key]
            if key not in sum_instead_of_average_set:
                scu_value /= len(scu_dict)
            response.append(
                {
                    "description": key,
                    "v": float("{:.1f}".format(scu_value)),
                    "t": time
                }
            )
        return response


    def define_color(self, avg, value):
        if abs(value - avg) > 0.2 * avg:
            return "RED"
        return "GREEN"


componentService = ComponentService()
