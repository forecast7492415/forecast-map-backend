from fastapi import FastAPI
from tortoise import Tortoise
from fastapi.middleware.cors import CORSMiddleware
from .routes import component, trend
from .config import settings
from .service import componentService
from fastapi_utilities import repeat_every

app = FastAPI()
app.include_router(component.router, prefix="/api/v1/components")
app.include_router(trend.router, prefix="/api/v1/trends")


origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event("startup")
async def init_db():
    await Tortoise.init(
        db_url=str(settings.database_url),
        modules=
        {'models':
            [
                'src.models'
            ]
        },
    )


@app.on_event("startup")
async def startup_event():
    await init_db()


@app.on_event('startup')
@repeat_every(seconds=60 * 15)
async def scheduling_task():
    await componentService.store_components_with_trends()




